#!/usr/bin/env bash

EPREFIX=${EPREFIX:=${HOME}/Gentoo}

echo "See if we have emerge in PATH..."
type -p emerge || exit 1

ack="${EPREFIX}/etc/portage/package.accept_keywords"

escaped_package=${1////\\/}
sed "/### START keyword for ${escaped_package}/,/### END keyword for ${escaped_package}/d" -i ${ack}

echo "### START keyword for $1" >> ${ack}

while ! emerge -pv $1; do
	emerge -pv $1 |& sed -r -n '/masked by: missing keyword/{s,^- (.*/.*)-[[:digit:]].*,\1 **,p;q}' | tee -a ${ack}
done

echo "### END keyword for $1" >> ${ack}
